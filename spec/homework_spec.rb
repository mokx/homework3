RSpec.describe Homework do
  it "has a version number" do
    expect(Homework::VERSION).not_to be nil
  end

  describe "when recipe called" do
    it "records a recipe" do

      Homework::Recipe.describe do
        recipe "Hard Boiled Egg" do
          ingredient "1 Egg"
          ingredient "1L Water"
          step "Place water in container"
          step "Place egg in water container"
          step "Boil water"
        end

        recipe "Bacon Surprise" do
          ingredient "3 strips of bacon"
          ingredient "1 ampalaya"
          ingredient "1 egg"
          ingredient "1 chili pepper"
          step "Mince egg"
          step "Cube ampalaya"
          step "Strip bacon"
          step "Sprinkle pepper"
          step "Call 911"
        end
      end
      expect(Homework::Recipe.for("Hard Boiled Egg").to_s).to eql("Hard Boiled Egg\n\nIngredients:\n1 Egg\n1L Water\n\nInstructions:\nPlace water in container\nPlace egg in water container\nBoil water\n")
      expect(Homework::Recipe.for("Bacon Surprise").to_s).to eql("Bacon Surprise\n\nIngredients:\n3 strips of bacon\n1 ampalaya\n1 egg\n1 chili pepper\n\nInstructions:\nMince egg\nCube ampalaya\nStrip bacon\nSprinkle pepper\nCall 911\n")
    end
    
    it "return nil when doesn't exisit" do
      expect(Homework::Recipe.for("does not exist")).to eql(nil)
    end
  end
end
