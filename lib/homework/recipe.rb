module Homework
  class Recipe
    attr_accessor :recipe_name, :ingredients, :steps
    @recipes = []
    @recipes.instance_eval do
      def [] key
        self.find { |recipe| recipe.recipe_name == key }
      end
    end

    def initialize(recipe_name:)
      @recipe_name = recipe_name
      @ingredients = []
      @steps = []
    end

    def to_s
      return <<~EOS
        #{@recipe_name}

        Ingredients:
        #{@ingredients.uniq.join("\n")}

        Instructions:
        #{@steps.uniq.join("\n")}
      EOS
    end

    class << self
      def describe &block
        instance_eval(&block) if block  
      end

      def for recipe_name
        @recipes[recipe_name]
      end

      private 
        def recipe recipe_name
          @current_recipe = recipe_name
          recipe = Recipe.new(recipe_name: recipe_name)
          @recipes << recipe
          yield if block_given?
        end

        def ingredient desc
          @recipes[@current_recipe].ingredients << desc
        end

        def step desc
          @recipes[@current_recipe].steps << desc
        end
    end 
  end
end
